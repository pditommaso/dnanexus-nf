# DNAnexus sandbox for NF

## Get starts 

The very first time create the app scheleton with: 

```
dx-app-wizard
```

To build and launch: 

```
dx login 
dx build nf-hello
dx run nf-hello
```

To update the app use 

```
dx build nf-hello -f
```